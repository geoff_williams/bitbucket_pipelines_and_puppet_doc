# Puppet Enterprise and Atlassian Bitbucket Pipelines

Bitbucket Pipelines represents the evolution of Bitbucket from being a simple GIT repository into an Enterprise level testing and version control solution.

Hosted within Bitbucket Cloud, Bitbucket Pipelines allows customers to create a one-stop-shop handling the end-to-end process of receiving, testing and deploying Puppet Code.

Integrating Bitbucket Pipelines with Puppet Enterprise is quick, easy and secure.  In a nutshell, the steps are:

1.  Developer checks in code to GIT
2.  Bitbucket Pipelines tests the code
3.  Bitbucket Pipelines runs a script to deploy the code to your Puppet Master

Bitbucket Pipelines is able to integrate existing test tools by executing them in-order and watching for exit status codes indicating test failure.  The deployment of code is handled by the Puppet Code Manager service (formerly R10K) which is responsible for ensuring the desired code is present on your Puppet Master.

## How Bitbucket Pipelines works
All testing is carried out inside a docker container which makes it easy to replicate and troubleshoot failing builds if required.  The steps forming a pipeline are described in the file `bitbucket-pipelines.yml` in the top level of a given repository.

Bitbucket Pipelines executes each step of the defined pipeline and returns the final status as the overall build status, with zero indicating a successful build and non-zero indicating failure.

To deploy code to a Puppet Master, the final step of the testing process is the use of the `curl` command to trigger the deployment but don't worry, there's a script to automate that.

## Getting Started
There are two main artefacts of code deployment for Puppet:  _Puppet Control Repositories_ and _Puppet Modules_.  The instructions are a little bit different for each one, see below for specific instructions.  For testing Puppet Modules, you have choice of a couple of different frameworks:

* [Testing Puppet Control Repositories with Onceover and Bitbucket Pipelines](onceover.md)
* [Testing Puppet modules with PDQTest and Bitbucket Pipelines](pdqtest.md)
* [Testing Puppet modules with RSpec-Puppet and Bitbucket Pipelines](rspec.md)
