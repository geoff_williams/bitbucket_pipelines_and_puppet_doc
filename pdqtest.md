# Testing Puppet modules with PDQTest and Bitbucket Pipelines

## PDQTest
[PDQTest](https://github.com/declarativesystems/pdqtest) provides a means to execute:

* Syntax validation
* Linting
* RSpec-Puppet testing
* Acceptance testing (doesn't work in Bitbucket Pipelines [yet](https://github.com/declarativesystems/pdqtest/issues/34))
* Documentation generation

All from a single command, `make`.

_With Bitbucket Pipelines, we can run tests for every `git push`.  Typically, modules are manually uploaded to the Puppet Forge and then referenced in your `Puppetfile` by version, so that the version you want gets deployed to your PuppetMaster._


## Getting started
It's recommended to install the PDQTest software to your local workstation (or a VM running locally if using Windows).  That way, you can iterately develop your Puppet module without having to wait for code to be uploaded and tested each time you change something.

Take a look at the following sections of the [PDQTest Manual](https://github.com/declarativesystems/pdqtest/#pdqtest-manual) to see how to get up and running as quickly as possible:

* [Installation Instructions](https://github.com/declarativesystems/pdqtest/blob/master/doc/installation.md)
* [Enabling Testing](https://github.com/declarativesystems/pdqtest/blob/master/doc/enabling_testing.md#enabling-testing)
* [Running Tests](https://github.com/declarativesystems/pdqtest/blob/master/doc/running_tests.md)
* [Generating tests](https://github.com/declarativesystems/pdqtest/blob/master/doc/test_generation.md)

If that all sounds too hard, here's a demo:

![demo](https://github.com/declarativesystems/pdqtest/raw/master/doc/demo.gif)


## Configuring Bitbucket Pipelines
Getting Bitbucket Pipelines running is as easy downloading a [bitbucket-pipelines.yml](https://bitbucket.org/geoff_williams/pdqtest_puppet_module/src/master/bitbucket-pipelines.yml?at=master&fileviewer=file-view-default) file to your project.

_If you have a Linux machine, you can do this easily from a terminal:_

```shell
wget https://bitbucket.org/geoff_williams/pdqtest_puppet_module/raw/master/bitbucket-pipelines.yml
```

This will configure Bitbucket Pipelines to run all logical tests the next time you `git push` with Pipelines enabled.

## Worked Example
You can see a complete working example at [https://bitbucket.org/geoff_williams/pdqtest_puppet_module](https://bitbucket.org/geoff_williams/pdqtest_puppet_module)
